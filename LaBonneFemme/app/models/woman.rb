class Woman < ApplicationRecord


  validates :name, presence:  true
  validates :surname, presence: true
  validates :age, presence: true
  validates :NumberPhone, presence: true

end
