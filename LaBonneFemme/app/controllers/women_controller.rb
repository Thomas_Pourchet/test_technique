class WomenController < ApplicationController


  def index
    @women = Woman.all
  end

  def show
    @woman = Woman.find(params[:id])
  end

  def search
    women = Woman.all
    search = params
    @women = []


    eyes = search[:color_eyes]
    hair = search[:color_hair]
    age = search[:age].to_i

    puts age

    if age == 0
      redirect_to root_path
    end

    women.each  do |elem|  #add in the array women_sort each woman'announce who correspond to the criteria

      if (elem[:color_eyes] == eyes || eyes == "toutes")  && ( elem[:color_hair] == hair || hair == "toutes" ) && (elem[:age] > age && elem[:age] < age + 10)
        @women << elem
      end
    end


  end

  def new
    @woman = Woman.new
  end

  def create
    @woman = Woman.new(params_woman)

    if @woman.save #save in database if only the form is correct
      if $flag
        $flag = false
      end
      redirect_to root_path, success: "Nouvelle annonce crée"
    else
      render 'new'
    end

  end

  private

  def params_woman
    params.require(:woman).permit(:name, :surname, :age, :NumberPhone,:color_eyes, :color_hair, :description)
  end
end
