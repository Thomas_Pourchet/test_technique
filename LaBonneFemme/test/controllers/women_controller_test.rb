require "test_helper"

class WomenControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get women_index_url
    assert_response :success
  end

  test "should get show" do
    get women_show_url
    assert_response :success
  end

  test "should get new" do
    get women_new_url
    assert_response :success
  end

  test "should get create" do
    get women_create_url
    assert_response :success
  end
end
