class CreateWomen < ActiveRecord::Migration[7.0]
  def change
    create_table :women do |t|
      t.string :name
      t.string :surname
      t.integer :age
      t.string :color_hair
      t.string :color_eyes
      t.string :description

      t.timestamps
    end
  end
end
