Rails.application.routes.draw do
  resources :women
  root "homes#index"
  get 'homes', to: "homes#index"
  get '/search', to: 'women#search', as: 'search'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
